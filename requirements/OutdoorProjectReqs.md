
# Outdoor Project

## Requirements (User Stories)

1. As a Hiker I want be able to find other people who are interested in going to the same hiking spot so I can go in a group.

2. As a Visitor I want to find cool outdoor locations near me so that I can go there and visit or camp.

3. As a User I would like to know which areas require parking passes so that when I visit I'll know the requirements and can buy the pass ahead of time and not get ticketed.

4. As a User who may not have English as a first language I'd like to be able to view the site successfully even though my English isn't very good.

5. As a member (logged in) 

## Bad Ones

1. As a member of the developer team I want to be able to easily add my code to the project code base and have it go live on the site.  [Not Valuable]

2. Let's make it so visitors can find a place to camp. [Not small, not in the right format, no specifics]

3. As a visitor to the site I want to be able to search for a campground so I can go camping and have fun. [This is an Epic (not small), but otherwise OK]

4. As a back-end developer I want a way to collect data about campgrounds so we can enter them in our database. [Not Valuable, wrong user, huge, not estimable, not testable ]

5. Fix the search bar bug where it fails if gibberish is input. [Not the right format, buxfix, yes or no? ]

6. Make the main landing page look good and not have a huge stupid banner ad for vodka. [How can you test for "looks good"?  Shouldn't really have a problem with this because we'll have good constant communication with stakeholders]

7. Do useability testing on the front page to see what works best. [Not the right format, not Valuable!]
