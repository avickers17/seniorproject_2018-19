﻿// Our Javascript goes here

console.log("In numbers.js");

// Callback function registered on a button.  Initiates AJAX call to
// retrieve random numbers from our server
$("#Request").click(function () {
    var a = $("#Count").val();
    console.log(a);
    var source = "/SimpleAPI/RandomNumbers/" + a;
    console.log(source);

    // Send an async request to our server, requesting JSON back
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: displayData,
        error: errorOnAjax 
    });
});

// data = {"message":"this is a message",
//          "num": "10",
//          "numbers": [50, 20, 22, 94]
//    }

// Display the data that we've retrieved
function displayData(data) {
    console.log(data);
    $("#Message").text(data["message"]);
    $("#Amount").text("Number of values requested " + data.num); // or data["num"]
    $("#Values").text(data.numbers);
    var sum = data.numbers.reduce(function (a, b) { return a + b; });
    var ave = sum / data.numbers.length;
    $("#Average").text("Average of these values is " + ave);

    var trace = {
        x: data.numbers1,
        y: data.numbers,
        mode: 'lines',
        type: 'scatter'
    };

    var plotData = [trace];
    var layout = {};
    Plotly.newPlot('theplot', plotData, layout);
}

function errorOnAjax()
{
    console.log("error");
}
