﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using User_WithDB.Models;

namespace User_WithDB.DAL
{
    public class UserContext : DbContext
    {
        public UserContext() : base("name=SecondTry")
        {

        }

        public virtual DbSet<User> Users { get; set; }
    }
}